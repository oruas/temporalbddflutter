import 'dart:io';
import 'dart:math';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/promesse.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/gps_db.dart';
import 'package:tuple/tuple.dart';
import 'package:geolocator/geolocator.dart';


/// Testing the POI attack and the Promesse LPPM on every user of the Cabspotting
/// dataset. We check that the protected trace has fewer POI than the raw one.
void testDistributionDistancePromesseCabspotting() {
  test('testing POI/Promesse on the Cabspotting dataset', () {
    double epsilon = 1000;
    var dir = Directory.current.path+'/datasets/';
    Map<int,List<Tuple3<double,double,double>>> gpsDataset = readCSV(dir+'cabspotting');
    bool b = true;
    List<double> timestamps;
    List<double> distances = [];
    Random rd = Random(0);
    int sizeRaw;
    int sizeModeled;
    int commonSize;
    for(var entry in gpsDataset.entries) {
      List<Tuple3<double, double, double>> gpsTraceUser = [];
      timestamps=[];
      GPSDB gpsModel = GPSDB();
      for (var point in entry.value.reversed) {
        gpsTraceUser.add(point);
        gpsModel.add(point.item1, point.item2, point.item3);
        timestamps.add(point.item1);
      }
      var modeledTrace = gpsModel.getTrace(timestamps);

      List<Tuple3<double, double, double>> gpsTraceUserPromesse = Promesse
          .smoothSpeed(gpsTraceUser, epsilon);

      List<Tuple3<double, double, double>> modeledTracePromesse = Promesse
          .smoothSpeed(modeledTrace, epsilon);

      sizeRaw = gpsTraceUserPromesse.length;
      sizeModeled = modeledTracePromesse.length;
      if (sizeRaw > sizeModeled) {
        int diff = sizeRaw - sizeModeled;
        for (int i=0; i<diff; i++) {
          int index = rd.nextInt(sizeRaw-i);
          gpsTraceUserPromesse.removeAt(index);
        }
      }
      if (sizeRaw < sizeModeled) {
        int diff = sizeModeled - sizeRaw;
        for (int i=0; i<diff; i++) {
          int index = rd.nextInt(sizeModeled-i);
          modeledTracePromesse.removeAt(index);
        }
      }
      commonSize = gpsTraceUserPromesse.length;
      for (int i=0; i<commonSize; i++) {
        distances.add(Geolocator.distanceBetween(
            gpsTraceUserPromesse[i].item2, gpsTraceUserPromesse[i].item3,
            modeledTracePromesse[i].item2, modeledTracePromesse[i].item3)
        );
      }

      // int diff = (gpsTraceUserPromesse.length - modeledTracePromesse.length).abs();
      // if (diff > 0) {
      //   maxDiff = max(maxDiff,diff);
      //   // debugPrint('raw: ${gpsTraceUserPromesse.length} / modeled: ${modeledTracePromesse.length}');
      //   // break;
      // }

      
/*
      distances += distributionDistancePOI(poiModeled,poiRaw);

      debugPrint('******** user ${entry.key} ********');
      debugPrint('Number of points:');
      debugPrint('Raw data: ${gpsTraceUser.length}');
      debugPrint('Model: ${modeledTrace.length}');
      debugPrint('Size of traces:');
      debugPrint('Raw data: $rawSize');
      debugPrint('Model: $modelSize');
      debugPrint('Gain: ${(100*(rawSize-modelSize))/rawSize}%');
      debugPrint('Number of POI of unprotected trace:');
      debugPrint('Raw data: ${poiRaw.length}');
      debugPrint('Modeled data: ${poiModeled.length}');
      debugPrint('Number of POI with Promesse:');
      debugPrint('Raw data: ${poiPromesseRaw.length}');
      debugPrint('Modeled data: ${poiPromesseModeled.length}');
      totalRaw += rawSize;
      totalModeled += modelSize;
      b = b && (poiRaw.length == poiModeled.length &&
          poiPromesseModeled.length == poiPromesseRaw.length);*/
    }
    List<Tuple2<double,double>> cumulDistrib = cumulativeDistribution(distances);
    writeResult(Directory.current.path+'/results/distribDistancePromesseTraceCabspotting.txt',cumulDistrib.toString());
    // debugPrint('Total gain: ${(100*(totalRaw-totalModeled))/totalRaw}%');
    expect(b, true);
  });
}


void main() {
  testDistributionDistancePromesseCabspotting();
}