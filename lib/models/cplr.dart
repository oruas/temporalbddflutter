library temporaldb;

import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';

import 'temporal_model.dart';

/// Connected Piecewise Linear Regression (CPLR) aims at modeling a time series
/// by successive linear regression.
///
/// Grützmacher, F., Beichler, B., Hein, A., Kirste, T., & Haubelt, C. (2018).
/// Time and memory efficient online piecewise linear approximation of sensor
/// signals. Sensors, 18(6), 1672.
///
/// Successive points are modeled by a straight line regression; several values
/// are kept, representing the parameters and errors of the current regression.
/// Thanks to those values, we do not need to keep the list of inserted points.

class CPLR implements TemporalModel {
  /// The starting time of the global model.
  double firstStartingTime = -1;

  /// The list of old models.
  /// Each model is represented by a tuple (t, v, a), where `t` is the starting
  /// time, `v` the associated value, also used as intercept, and `a` is the
  /// polynomial, i.e. v_ = a(t_-t) + v.
  List<Tuple3<double, double, double>> listOldModels = [];

  /// Variables for the current linear regression

  /// The starting time of the current model.
  double currentStartingTime = 0;

  /// The intercept of the current model.
  double currentStartingValue = 0;

  int n = 0;
  double y = 0;
  double x2N = 0;
  double xyN = 0;
  double y2N = 0;
  double betaN = 0;
  double ssrN = 0;

  /// The last inserted point, used to start the new model.
  double lastTime = 0;
  double lastValue = 0;

  /// The maximum tolerated error for the whole model.
  double maxError = 0.01;

  /// TODO implement
  @override
  void add(double t, double v) {
    if (firstStartingTime == -1) {
      firstStartingTime = t;
      _initModel(t, v);
      return;
    }

    return;
  }

  /// TODO implement
  void _initModel(double t, double v) {}

  ///TODO complete
  @override
  List<List<double>> getModels() {
    List<List<double>> models = [];
    for (var tuple3 in listOldModels) {
      models.add([tuple3.item3, tuple3.item2]);
    }
//    models.add([currentA, currentStartingValue]);
    return models;
  }

  @override
  int getNbModels() {
    return (listOldModels.length + 1);
  }

  @override
  int getSize() {
    return (9 + 3 * listOldModels.length);
  }

  @override
  List<double> getTimestamps() {
    List<double> timestamps = [];
    for (var tuple3 in listOldModels) {
      timestamps.add(tuple3.item1);
    }
    timestamps.add(currentStartingTime);
    return timestamps;
  }

  @override
  List<Tuple2<double, List<double>>> getModelsAndTimestamps() {
    List<Tuple2<double, List<double>>> modelsAndTimestamps = [];
    for (var tuple3 in listOldModels) {
      modelsAndTimestamps
          .add(Tuple2(tuple3.item1, [tuple3.item3, tuple3.item2]));
    }
    return modelsAndTimestamps;
  }

  @override
  String getType() {
    return "CPLR";
  }

  ///TODO complete
  @override
  double read(double t) {
    /// If the reading time is before the time of the first model, we throw.
    if (firstStartingTime == -1 || t < firstStartingTime) {
      throw ErrorDescription("Error the time is unavailable");
    }
    if (currentStartingTime <= t) {
      // return _getValue(t, currentStartingTime, currentA, currentStartingValue);
    }
    for (var tuple3 in listOldModels.reversed) {
      if (tuple3.item1 <= t) {
        // return _getValue(t, tuple3.item1, tuple3.item3, tuple3.item2);
      }
    }

    /// This code should be unreachable thanks to the first `if` condition.
    throw ErrorDescription("Error the time is unavailable (!)");
  }

  /// TODO update
  /// Returns the estimated value for t by the polynomial Ax+B, centered in T,
  /// i.e. A(t-T)+B.
  static double _getValue(double t, double T, double A, double B) {
    return (A * (t - T) + B);
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "error":
          {
            maxError = double.parse(value);
          }
          break;
      }
    });
  }

  ///TODO update
  @override
  String toString() {
    String s = "CPLR:\n";
    s += "[";
    for (var tuple3 in listOldModels) {
      s += "\t" +
          tuple3.item1.toString() +
          ": " +
          tuple3.item3.toString() +
          " " +
          tuple3.item2.toString() +
          "\n";
    }
//    s += "\t"+currentStartingTime.toString() + ": " + currentA.toString() + " " + currentStartingValue.toString() + "\n";
    s += "]";
    return s;
  }
}
