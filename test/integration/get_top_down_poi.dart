import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/poi.dart';
import 'package:temporaldb/geo_example/promesse.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/gps_db.dart';
import 'package:temporaldb/platform.dart';
import 'package:tuple/tuple.dart';

/// This code sample is meant to be run on smartphones, to evaluate the speed of
/// the Divide&Stay (D&S) approach and Promesse protection.
///
/// To do so, one must execute this file from the example app directory:
/// ~$ flutter run -d [device_id] ../test/integration/get_top_down_poi.dart
///
/// ## POI computation times:
///
/// #### Before Promesse protection
///
/// * Fairphone 3: 109215/109323/109226/109199/109174
/// * Pixel 7 Pro: 29890/29987/29996/29953/29984
/// * Moto Z: 114512/114364/114427/114479/114305
/// * iPhone 12: 18880/18895/18848/18764/18482
/// * iPhone 14 Plus: 19336/19345/19333/19376/19368
///
/// #### After Promesse protection
///
/// * Fairphone 3: 99/98/97/97/80
/// * Pixel 7 Pro: 28/22/41/76/29
/// * Moto Z: 114/110/117/127/115
/// * iPhone 12: 20/15/15/15/14
/// * iPhone 14 Plus: 16/14/14/17/14
///
/// ## Promesse computation times:
///
/// * Fairphone 3: 1332/1274/1275/1279/1281
/// * Pixel 7 Pro: 368/376/361/363/363
/// * Moto Z: 1470/1451/1443/1454/1450
/// * iPhone 12: 238/237/235/234/226
/// * iPhone 14 Plus: 221/224/227/227/227
///
void main() async {
  TestWidgetsFlutterBinding.ensureInitialized();

  // List of timestamps to regenerate the trace
  List<double> timestamps= [];

  // Load dataset file
  var dir = await localPath+'/datasets/';
  List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir + 'privamov-gps-user1', 1);

  // Model trace
  GPSDB gpsModel = GPSDB(0, {'error': '0.001'});
  for (var point in gpsTraceUser) {
    gpsModel.add(point.item1, point.item2, point.item3);
    timestamps.add(point.item1);
  }
  var modeledTrace = gpsModel.getTrace(timestamps);

  computePOIs(modeledTrace);
  var protectedTrace = computePromesseProtection(modeledTrace);
  computePOIs(protectedTrace);
}

void computePOIs(List<Tuple3<double, double, double>> modeledTrace) async {
  double minTime = 60*5;
  double minDistance = 500;
  int minPts = 2;

  final stopwatch = Stopwatch();
  stopwatch.start();
  POI.getTopDownPOI(modeledTrace, minTime, minDistance, minPts);
  stopwatch.stop();
  var t2 = stopwatch.elapsedMilliseconds;

  debugPrint('getTopDownPOI done in: $t2');
}

List<Tuple3<double, double, double>> computePromesseProtection(List<Tuple3<double, double, double>> modeledTrace) {
  // Distance (m) between two GPS points in the anonymized trace produced by
  // Promesse
  const double _epsilon = 500;

  final stopwatch = Stopwatch();
  stopwatch.start();
  List<Tuple3<double, double, double>> modeledTracePromesse = Promesse
      .smoothSpeed(modeledTrace, _epsilon);
  stopwatch.stop();

  debugPrint('Promesse done in: ${stopwatch.elapsedMilliseconds}');
  return modeledTracePromesse;
}