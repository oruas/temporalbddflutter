import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';


int typeModel = 5; /// 5 -> FLISW

double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}

double getMAEWithoutW0(List<double> coeff1, List<double> coeff2) {
  double mae = 0;
  for(var i=0; i<coeff1.length-1; i++) {
    mae = mae + (coeff1[i]-coeff2[i]).abs();
  }
  return mae;
}
double getRMSEWithoutC0(List<double> coeff1, List<double> coeff2) {
  double rmse = 0;
  double a;
  for(var i=0; i<coeff1.length-1; i++) {
    a=(coeff1[i]-coeff2[i]);
    rmse = rmse + a*a;
  }
  return sqrt(rmse);
}

double getMAE(List<double> coeff1, List<double> coeff2) {
  double mae = 0;
  for(var i=0; i<coeff1.length; i++) {
    mae = mae + (coeff1[i]-coeff2[i]).abs();
  }
  return mae;
}
double getRMSE(List<double> coeff1, List<double> coeff2) {
  double rmse = 0;
  double a;
  for(var i=0; i<coeff1.length; i++) {
    a=(coeff1[i]-coeff2[i]);
    rmse = rmse + a*a;
  }
  return sqrt(rmse);
}

bool arePredictionCorrect(TemporalModel model, List<double> listX, List<double> listY, double error) {
  for (int i=0; i<listX.length; i++) {
    double t = listX[i];
    double v = listY[i];
    double prediction = model.read(t);
    if(!((prediction-v).abs() <= v.abs()*error)){
      debugPrint('WRONG PREDICTION: $t $v $prediction ${(prediction-v).abs()} ${(prediction-v).abs()/v.abs()}');
      return false;
    }
  }
  return true;
}

TemporalModel getModel(int type, double error, double tnorm, double ynorm) {
  Map<String,String> parameters = {
    'error':error.toString(),
    'timeValueNormalization':tnorm.toString(),
    'yValueNormalization':ynorm.toString(),
  };
  return (Factory(type,parameters));
}
TemporalModel getModelError(int type, double error) {
  Map<String,String> parameters = {
    'error':error.toString(),
  };
  return (Factory(type,parameters));
}






void testDegree2Normalization() {
  List<List<double>> polynoms = [
    [42.14,125.52,321.23],
    [9.28,2.17,2.41],
    [-1.24,9.72,12.2],
    [9.30,-6.23,-23.4],
    [-32.56,-238.95,-241.2],
    [-93.72,-3.47,123.23],
    /// Same but with 100x coefficients
    [4214,12552,32123],
    [928,217,241],
    [-124,972,122],
    [930,-623,-234],
    [-3256,-23895,-2412],
    [-9372,-347,123]
  ];
  int nbPointsAdd = 200;
  double errorRatio = 0.00001;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  test('Basic test the degree 2 with normalization', () {
    /// We tests several polynoms
    counter = 0;
    for (var polynomCoeffs in polynoms) {
      TemporalModel model = getModel(typeModel, errorRatio, pow(2,15).toDouble(), pow(2,15).toDouble());
      listT = [];
      listV = [];
      t = 1000; //1000000;
      for(var i=0; i<nbPointsAdd; i++) {
        v = getValuePoly(polynomCoeffs, t);
        listT.add(t);
        listV.add(v);
        // debugPrint('Adding point number $counter: $t, $v');
        model.add(t, v);
        t = t + 10000;
      }
      nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
      coeff = model.getModels().last;
      degree = coeff.length-1;
      bool good = (nbModels == 1 && degree == 2);
      if(good) {
        mae = getMAEWithoutW0(coeff, polynomCoeffs);
        rmse = getRMSEWithoutC0(coeff, polynomCoeffs);
        debugPrint('Polynom has the good degree :');
        debugPrint('MAE: $mae');
        debugPrint('RMSE: $rmse');
        debugPrint('Expected polynomCoeffs: ${polynomCoeffs.toString()}, obtained: ${coeff.toString()}');
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 1)');
        debugPrint('Expected polynom: ${polynomCoeffs.toString()}');
        debugPrint('Degree: $degree (should be 2)');
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Degree 2 polynomCoeffs $counter OK');
      counter++;
    }
  }); /// end of test
}

void testDegree2() {
  List<List<double>> polynoms = [
    [42.14,125.52,321.23],
    [9.28,2.17,2.41],
    [-1.24,9.72,12.2],
    [9.30,-6.23,-23.4],
    [-32.56,-238.95,-241.2],
    [-93.72,-3.47,123.23],
    /// Same but with 100x coefficients
    [4214,12552,32123],
    [928,217,241],
    [-124,972,122],
    [930,-623,-234],
    [-3256,-23895,-2412],
    [-9372,-347,123]
  ];
  int nbPointsAdd = 200;
  double errorRatio = 0.01;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  test('Basic test the degree 2 with t0=0 and no normalization', () {
    /// We tests several polynoms
    counter = 0;
    debugPrint('Starting polynom $counter...');
    for (var polynomCoeffs in polynoms) {
      TemporalModel model = getModel(typeModel, errorRatio, 1.0, 1.0);
      listT = [];
      listV = [];
      t = 0;
      debugPrint('Polynom initialized');
      for(var i=0; i<nbPointsAdd; i++) {
        v = getValuePoly(polynomCoeffs, t);
        listT.add(t);
        listV.add(v);
        // debugPrint('Adding point number $counter: $t, $v');
        model.add(t, v);
        t = t + 470; /// Maximum value for which tests pass
        if(i % 20 == 0) {
          debugPrint('$i points added');
        }
      }
      debugPrint('Points added');
      nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
      coeff = model.getModels().last;
      degree = coeff.length-1;
      bool good = (nbModels == 1 && degree == 2);
      if(good) {
        mae = getMAE(coeff, polynomCoeffs);
        rmse = getRMSE(coeff, polynomCoeffs);
        debugPrint('Polynom has the good degree :');
        debugPrint('MAE: $mae');
        debugPrint('RMSE: $rmse');
        debugPrint('Expected polynomCoeffs: ${polynomCoeffs.toString()}, obtained: ${coeff.toString()}');
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 1)');
        debugPrint('Expected polynom: ${polynomCoeffs.toString()}');
        debugPrint('Degree: $degree (should be 2)');
        PolyFit p = PolyFit(Array(listT), Array(listV), degree);
        debugPrint('PolyFit polynom:');
        debugPrint(p.coefficients().toString());
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Degree 2 polynomCoeffs $counter OK');
      counter++;
    }
  }); /// end of test
}










void testDegree1Normalization() {
  List<List<double>> polynoms = [
    [42.14,125.52],
    [9.28,2.17],
    [-1.24,9.72],
    [9.30,-6.23],
    [-32.56,-238.95],
    [-93.72,-3.47],
    /// Same but with 100x coefficients
    [4214,12552],
    [928,217],
    [-124,972],
    [930,-623],
    [-3256,-23895],
    [-9372,-347]
  ];
  int nbPointsAdd = 200;
  double errorRatio = 0.00001;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  test('Basic test the degree 1 with normalization', () {
    /// We tests several polynoms
    counter = 0;
    for (var polynomCoeffs in polynoms) {
      TemporalModel model = getModel(typeModel, errorRatio, pow(2,22).toDouble(), pow(2,22).toDouble());
      listT = [];
      listV = [];
      t = 1000000;
      for(var i=0; i<nbPointsAdd; i++) {
        v = getValuePoly(polynomCoeffs, t);
        listT.add(t);
        listV.add(v);
        // debugPrint('Adding point number $counter: $t, $v');
        model.add(t, v);
        t = t + 10000;
      }
      nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
      coeff = model.getModels().last;
      degree = coeff.length-1;
      bool good = (nbModels == 1 && degree == 1);
      if(good) {
        mae = getMAEWithoutW0(coeff, polynomCoeffs);
        rmse = getRMSEWithoutC0(coeff, polynomCoeffs);
        debugPrint('Polynom has the good degree :');
        debugPrint('MAE: $mae');
        debugPrint('RMSE: $rmse');
        debugPrint('Expected polynomCoeffs: ${polynomCoeffs.toString()}, obtained: ${coeff.toString()}');
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 1)');
        debugPrint('Degree: $degree (should be 1)');
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Degree 1 polynomCoeffs $counter OK');
      counter++;
    }
  }); /// end of test
}

void testDegree1() {
  List<List<double>> polynoms = [
    [42.14,125.52],
    [9.28,2.17],
    [-1.24,9.72],
    [9.30,-6.23],
    [-32.56,-238.95],
    [-93.72,-3.47],
    /// Same but with 100x coefficients
    [4214,12552],
    [928,217],
    [-124,972],
    [930,-623],
    [-3256,-23895],
    [-9372,-347]
    ];
  int nbPointsAdd = 200;
  double errorRatio = 0.00001;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  test('Basic test the degree 1 with t0=0 and no normalization', () {
    /// We tests several polynoms
    counter = 0;
    for (var polynomCoeffs in polynoms) {
      TemporalModel model = getModel(typeModel, errorRatio, 1.0, 1.0);
      listT = [];
      listV = [];
      t = 0;
      for(var i=0; i<nbPointsAdd; i++) {
        v = getValuePoly(polynomCoeffs, t);
        listT.add(t);
        listV.add(v);
        // debugPrint('Adding point number $counter: $t, $v');
        model.add(t, v);
        t = t + 10000;
      }
      nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
      coeff = model.getModels().last;
      degree = coeff.length-1;
      bool good = (nbModels == 1 && degree == 1);
      if(good) {
        mae = getMAE(coeff, polynomCoeffs);
        rmse = getRMSE(coeff, polynomCoeffs);
        debugPrint('Polynom has the good degree :');
        debugPrint('MAE: $mae');
        debugPrint('RMSE: $rmse');
        debugPrint('Expected polynomCoeffs: ${polynomCoeffs.toString()}, obtained: ${coeff.toString()}');
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 1)');
        debugPrint('Degree: $degree (should be 1)');
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Degree 1 polynomCoeffs $counter OK');
      counter++;
    }
  }); /// end of test
}








void testDegree0() {
  List<double> polynoms = [-21948725.4561,-51324.1533,-2314,0,12422.3214,45985123.1234,1259875239.1234];
  int nbPointsAdd = 200;
  double errorRatio = 0.00001;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  test('Basic test the degree 0', () {
    /// We tests several polynoms
    counter = 0;
    for (var polynom in polynoms) {
      TemporalModel model = getModelError(typeModel, errorRatio);
      listT = [];
      listV = [];
      t = 100000000;
      for(var i=0; i<nbPointsAdd; i++) {
        t = t + 1000;
        v = getValuePoly([polynom], t);
        listT.add(t);
        listV.add(v);
        // debugPrint('Adding point number $counter: $t, $v');
        model.add(t, v);
      }
      nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
      coeff = model.getModels().last;
      if(coeff.isEmpty) {
        coeff = [0];
      }
      degree = coeff.length-1;
      bool good = (nbModels == 1 && degree == 0);
      if(good) {
        mae = getMAE(coeff, [polynom]);
        rmse = getRMSE(coeff, [polynom]);
        debugPrint('Polynom has the good degree :');
        debugPrint('MAE: $mae');
        debugPrint('RMSE: $rmse');
        debugPrint('Expected polynom: [$polynom], obtained: [${coeff[0]}]');
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 1)');
        debugPrint('Degree: $degree (should be 0)');
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Degree 0 polynom $counter OK');
      counter++;
    }
  }); /// end of test

}


void main() {
  testDegree0();
  testDegree1();
  testDegree1Normalization();
  testDegree2();
  testDegree2Normalization();
}



