# Temporal database package

temporaldb is an experimental library for flutter.

Its main purpose is to provide temporal databases to allow mobile devices able to store an important
quantity of data. By using models, this quantity is potentially unlimited.

As a use case, the library also contains location privacy attacks and protection mechanisms and
an application use to store the values of the accelerometer values using the provided model.

## MODELS

In order to store efficiently temporal data, the library includes several modeling techniques:
- Fast Linear Interpolation (FLI)
- Sliding Windows And Bottom-up (SWAB)
- Greycat

Not only we are the first to port SWAB and Greycat to mobile devices, we propose FLI, a storage
system based on a fast PLA to model temporal data.

## Location privacy
In order to show that the modeled temporal data still possess its utility, we provide location
privacy attacks and protection mechanisms. The attacks infers the Points of Interests (POI) from
the trace and Promesse protects the trace by modifying the trace in order to prevent such an attack.


## Getting started
To use a given model you can use the model directly (in ./models/) or use the Factory class in
temporalDB_1D_factory.dart
The default type is FLAIR (value 0), otherwise:
- 1 for SWAB
- 2 for Greycat

## Example Apps
The following applications are provided as example and used to benchmark our library.
* [Accelerometer](https://gitlab.inria.fr/oruas/temporaldbflutter/-/tree/main/example): example application of the lib, stores the accelerometer values using FLI.
* [Benchmarking against SQL](https://gitlab.inria.fr/oruas/benchmarking_memory_space): application made for benchmarking FLI and SQLite for memory and space on basic example (insertion of random or constant values).
* [in-situ LPPM](https://gitlab.inria.fr/oruas/in-situ-lppm): application which uses FLI to do POI attacks and uses Promesse, everything in-situ.
* [Benchmarking against SWAB et Greycat](https://gitlab.inria.fr/oruas/benchmarking_throughput): application which compares the number of insertions/reads of FLI versus SWAB and Greycat.
in-situ LPPM requires two datasets to work: Cabspotting and PrivaMov.


Additional parameters (such as the tolerated error) can be provided in a map:
`Factory myModel = Factory(0, {'error':'0.001'});`
