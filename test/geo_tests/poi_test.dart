import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/poi.dart';
import 'package:temporaldb/geo_example/promesse.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:tuple/tuple.dart';
import 'package:collection/collection.dart';


/// Testing the read functions on an artificial dataset
void testReadCSVArtificialDataset() {
  test('testing reading Artificial Dataset', ()
  {
    var dir = Directory.current.path+'/datasets/';
    Map<int, List<Tuple3<double,double,double>>> gpsTrace = readCSV(dir+'artificial_dataset.txt');
    List<Tuple3<double,double,double>> gpsTraceUser0 = readCSVUser(dir+'artificial_dataset.txt',0);
    List<Tuple3<double,double,double>> gpsTraceUser1 = readCSVUser(dir+'artificial_dataset.txt',1);

    List<Tuple3<double,double,double>> answerUser0 = [
      const Tuple3(1.0, 0.0, 0.0), const Tuple3(2.0, 0.0, 1.0), const Tuple3(3.0, 0.0, 2.0),
      const Tuple3(4.0, 0.0, 3.0), const Tuple3(5.0, 0.0, 4.0), const Tuple3(6.0, 0.0, 5.0),
      const Tuple3(7.0, 0.0, 6.0), const Tuple3(8.0, 0.0, 7.0), const Tuple3(9.0, 0.0, 8.0),
      const Tuple3(10.0, 0.0, 9.0), const Tuple3(11.0, 0.0, 10.0)
    ];
    List<Tuple3<double,double,double>> answerUser1 = [
      const Tuple3(12.0, 0.0, 0.0), const Tuple3(13.0, 1.0, 0.0), const Tuple3(14.0, 2.0, 0.0),
      const Tuple3(15.0, 3.0, 0.0), const Tuple3(16.0, 4.0, 0.0), const Tuple3(17.0, 5.0, 0.0),
      const Tuple3(18.0, 6.0, 0.0), const Tuple3(19.0, 7.0, 0.0), const Tuple3(20.0, 8.0, 0.0),
      const Tuple3(21.0, 9.0, 0.0), const Tuple3(22.0, 10.0, 0.0)
    ];


    Map<int, List<Tuple3<double,double,double>>> answerAll = {
      0 : answerUser0,
      1 : answerUser1
    };

    Function eq = const ListEquality().equals;
    Function mapEq = const DeepCollectionEquality().equals;
    expect(eq(answerUser0,gpsTraceUser0) && eq(answerUser1,gpsTraceUser1) && mapEq(answerAll,gpsTrace),true);
  });
}

/// testing the POI attack on a single user
void testPOI() {
  test('testing POI attack on a single user', ()
  {
    double minTime = 60*5;
    double minDistance = 500;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    // List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'privamov-gps-user1-sampling10',1);
    List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'cabspotting-user0',0);
    // debugPrint('dataset loaded: ${gpsTraceUser.length} points.');
    var poi = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    // debugPrint(poi.length);
    expect(poi.isNotEmpty,true);
  });
}

/// Testing the Promesse LPPM on a single user
void testPromesse() {
  test('testing Promesse LPPM on a single user', () {
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'cabspotting',32);
    // List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'privamov-gps-user1-sampling10',1);
    var poi = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> gpsTraceUserPromesse = Promesse.smoothSpeed(gpsTraceUser, epsilon);
    var poiPromesse = POI.getPOI(gpsTraceUserPromesse, minTime, minDistance, minPts);

/*    writeResult(Directory.current.path+'/results/trace.txt', gpsTraceUser.toString());
    writeResult(Directory.current.path+'/results/tracePromesse.txt', gpsTraceUserPromesse.toString());
    writeResult(Directory.current.path+'/results/resultsPOI.txt', poi.toString());
    writeResult(Directory.current.path+'/results/resultsPOIPromesse.txt', poiPromesse.toString());*/

    debugPrint(gpsTraceUser.sublist(0,30).toString());
    debugPrint(gpsTraceUserPromesse.sublist(0,30).toString());
    // debugPrint(poi.toString());
    // debugPrint(poiPromesse.toString());
    debugPrint('Number of points in traces:');
    debugPrint('Raw data: ${gpsTraceUser.length}');
    debugPrint('With Promesse: ${gpsTraceUserPromesse.length}');
    debugPrint('Number of POI:');
    debugPrint('Raw data: ${poi.length}');
    debugPrint('With Promesse: ${poiPromesse.length}');
    expect(poi.length>poiPromesse.length || poi.isEmpty ,true);
  });
}

/// Testing that the trace is sorted
void testOrderTrace() {
  test('testing the order of the trace', ()
  {
    var dir = Directory.current.path + '/datasets/';
    List<Tuple3<double, double, double>> gpsTraceUser = readCSVUser(
        dir + 'cabspotting', 32);
    double timestamp = gpsTraceUser[0].item1;
    bool alwaysIncreasing = true;
    bool alwaysDecreasing = true;
    for(var v in gpsTraceUser) {
      alwaysIncreasing = alwaysIncreasing && v.item1 >= timestamp;
      alwaysDecreasing = alwaysDecreasing && v.item1 <= timestamp;
    }
    debugPrint('$alwaysIncreasing');
    debugPrint('$alwaysDecreasing');
    expect(alwaysIncreasing || alwaysDecreasing, true);

    double epsilon = 1000;
    List<Tuple3<double, double, double>> gpsTraceUserPromesse = Promesse
        .smoothSpeed(gpsTraceUser, epsilon);
    alwaysIncreasing = true;
    alwaysDecreasing = true;
    for(var v in gpsTraceUserPromesse) {
      alwaysIncreasing = alwaysIncreasing && v.item1 >= timestamp;
      alwaysDecreasing = alwaysDecreasing && v.item1 <= timestamp;
    }
    debugPrint('$alwaysIncreasing');
    debugPrint('$alwaysDecreasing');
    expect(alwaysIncreasing || alwaysDecreasing, true);
  });
}

/// Testing the POI attack and the Promesse LPPM on every user of the Cabspotting
/// dataset. We check that the protected trace has fewer POI than the raw one.
void testPromesseCabspotting() {
  test('testing POI/Promesse on the Cabspotting dataset', () {
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    Map<int,List<Tuple3<double,double,double>>> gpsDataset = readCSV(dir+'cabspotting');
    for(var entry in gpsDataset.entries) {
      var poi = POI.getPOI(entry.value, minTime, minDistance, minPts);
      List<Tuple3<double, double, double>> gpsTraceUserPromesse = Promesse
          .smoothSpeed(entry.value, epsilon);
      var poiPromesse = POI.getPOI(
          gpsTraceUserPromesse, minTime, minDistance, minPts);
      if(poi.isNotEmpty && poi.length <= poiPromesse.length) {
        debugPrint('user: ${entry.key}');
        debugPrint('number of POI: ${poi.length}');
        debugPrint('number of POI with prom: ${poiPromesse.length}');
      }
      expect(poi.isEmpty || poi.length > poiPromesse.length, true);
    }
  });
}

/// Testing POI/Promesse on a subset of the trace of a single user
/// Was used to debug POI: not useful anymore
void testPromesseLimit() {
  test('testing POI attack', () {
    // int limit = 25862;
    // int limit = 25839; /// It works until 25838 included -> error starting at 25840
    // int nb_out = 25862-25839; /// 23 ?
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    List<Tuple3<double,double,double>> gpsTraceUser_ = readCSVUser(dir+'cabspotting',32);
    // var gpsTraceUser = gpsTraceUser_.sublist(0,limit);
    // var gpsTraceUser = gpsTraceUser_.sublist(limit-10000,25862);
    // var gpsTraceUser = gpsTraceUser_.sublist(25862-limit-1,25862); /// Error -> size ???
    var gpsTraceUser = gpsTraceUser_.sublist(0,25862~/2-3) + gpsTraceUser_.sublist(25862~/2+3,25862); /// -3 +3 pass but not -2 +2
    var poi = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> gpsTraceUserPromesse = Promesse.smoothSpeed(gpsTraceUser, epsilon);
    var poiPromesse = POI.getPOI(gpsTraceUserPromesse, minTime, minDistance, minPts);

    debugPrint(gpsTraceUser_.sublist(25839,25862).toString());

/*    writeResult(Directory.current.path+'/results/trace.txt', gpsTraceUser.toString());
    writeResult(Directory.current.path+'/results/tracePromesse.txt', gpsTraceUserPromesse.toString());
    writeResult(Directory.current.path+'/results/resultsPOI.txt', poi.toString());
    writeResult(Directory.current.path+'/results/resultsPOIPromesse.txt', poiPromesse.toString());*/

    // debugPrint(gpsTraceUser.sublist(0,30).toString());
    // debugPrint(gpsTraceUserPromesse.sublist(0,30).toString());
    // debugPrint(poi.toString());
    // debugPrint(poiPromesse.toString());
    debugPrint('Number of points in traces:');
    debugPrint('Raw data: ${gpsTraceUser.length}');
    debugPrint('With Promesse: ${gpsTraceUserPromesse.length}');
    debugPrint('Number of POI:');
    debugPrint('Raw data: ${poi.length}');
    debugPrint('With Promesse: ${poiPromesse.length}');
    expect(poi.length>poiPromesse.length || poi.isEmpty ,true);
  });
}


void main() {
  testReadCSVArtificialDataset();
  testOrderTrace();
  // testPOI();
  testPromesse();
  testPromesseCabspotting();
  // testPromesseLimit();
}