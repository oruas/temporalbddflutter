import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/poi.dart';
import 'package:temporaldb/geo_example/promesse.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/gps_db.dart';
import 'package:tuple/tuple.dart';


void testPOIExample(int user) {
  test('testing the POI attack and Promesse LPPM on a single user', () {
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    List<Tuple3<double,double,double>> gpsTraceUser_ = readCSVUser(dir+'cabspotting',user);
    String outputAdd = Directory.current.path+'/results/POI-cabspotting-user-$user/';
    Directory(outputAdd).createSync(recursive: true);

    List<Tuple3<double,double,double>> gpsTraceUser = [];
    GPSDB gpsModel = GPSDB(0,{'error':'0.001'});
    List<double> timestamps = [];

    for(var point in gpsTraceUser_.reversed) { /// reversed for cabspotting
      gpsTraceUser.add(point);
      gpsModel.add(point.item1, point.item2, point.item3);
      timestamps.add(point.item1);
    }
    var modeledTrace = gpsModel.getTrace(timestamps);

    var poiRaw = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    var poiRawTopDown = POI.getTopDownPOI(gpsTraceUser, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> gpsTraceUserPromesse = Promesse.smoothSpeed(gpsTraceUser, epsilon);
    var poiPromesseRaw = POI.getPOI(gpsTraceUserPromesse, minTime, minDistance, minPts);
    var poiPromesseRawTopDown = POI.getTopDownPOI(gpsTraceUserPromesse, minTime, minDistance, minPts);

    var poiModeled = POI.getPOI(modeledTrace, minTime, minDistance, minPts);
    var poiModeledTopDown = POI.getTopDownPOI(modeledTrace, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> modeledTracePromesse = Promesse.smoothSpeed(modeledTrace, epsilon);
    var poiPromesseModeled = POI.getPOI(modeledTracePromesse, minTime, minDistance, minPts);
    var poiPromesseModeledTopDown = POI.getTopDownPOI(modeledTracePromesse, minTime, minDistance, minPts);

    writeResult(outputAdd+'poiRaw.txt', poiRaw.toString());
    writeResult(outputAdd+'poiRawTopDown.txt', poiRawTopDown.toString());
    writeResult(outputAdd+'poiRawPromesse.txt', poiPromesseRaw.toString());
    writeResult(outputAdd+'poiRawPromesseTopDown.txt', poiPromesseRawTopDown.toString());
    writeResult(outputAdd+'poiModeled.txt', poiModeled.toString());
    writeResult(outputAdd+'poiModeledTopDown.txt', poiModeledTopDown.toString());
    writeResult(outputAdd+'poiModeledPromesse.txt', poiPromesseModeled.toString());
    writeResult(outputAdd+'poiModeledPromesseTopDown.txt', poiPromesseModeledTopDown.toString());

    writeResult(outputAdd + 'trace100.txt', gpsTraceUser.sublist(0,100).toString());
    writeResult(outputAdd + 'trace100.txt', modeledTrace.sublist(0,100).toString());

    debugPrint('POIs Raw: ${poiRaw.length}');
    debugPrint('POIs Raw TopDown: ${poiRawTopDown.length}');
    debugPrint('POIs Raw Promesse : ${poiPromesseRaw.length}');
    debugPrint('POIs Raw Promesse TopDown : ${poiPromesseRawTopDown.length}');

    debugPrint('POIs Modeled: ${poiModeled.length}');
    debugPrint('POIs Modeled TopDown: ${poiModeledTopDown.length}');
    debugPrint('POIs Modeled Promesse : ${poiPromesseModeled.length}');
    debugPrint('POIs Modeled Promesse TopDown : ${poiPromesseModeledTopDown.length}');

    //bool b = (poiRaw.length == poiModeled.length && poiPromesseModeled.length == poiPromesseRaw.length);

    expect(true,true);
  });
}




void main() {
  testPOIExample(0);
}