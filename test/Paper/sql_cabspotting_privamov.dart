import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/platform.dart';
import 'package:tuple/tuple.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import 'package:path/path.dart';


const String _addResults='results_memory/';

void testCabspottingSQL() async {
  test('Benchmarking memory space used by Cabspotting with and without FLAIR', ()
  async {

    ///Opening file for writing results
    String _resultsFileName = 'cabspottingSQLResults';
    final File outputFile = await localDirFile(_addResults,_resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tFileSize\t");

    const _sqliteDBFileName = 'sqlite_database.db';

    ///opening the sqlite database
    WidgetsFlutterBinding.ensureInitialized();
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
    final Future<Database> database = openDatabase(
    join(await getDatabasesPath(), _sqliteDBFileName),
    onCreate: (db, version) { return db.execute( "CREATE TABLE RDpoints(index REAL PRIMARY KEY, user INTEGER, timestamp REAL, lat REAL, lng REAL)"); },
    version: 1,
    );
    final Database db = await database;
    var dbBatch = db.batch();


    Map<int, List<Tuple3<double, double, double>>> gpsDataset = {};
    try {
      gpsDataset = readCSV(Directory.current.path+'/datasets/cabspotting');
    }
    catch (e) {
      debugPrint(e as String?);
      return;
    }

    int index=0;
    for(var entry in gpsDataset.entries) {
      for (var point in entry.value.reversed) {
        /// Adding in SQL
        dbBatch.insert(
          'RDpoints',
          {'index': index,
            'user': entry.key,
            'timestamp': point.item1,
            'lat' : point.item2,
            'lng' : point.item3},
          conflictAlgorithm: ConflictAlgorithm.replace,);
        index++;
      }

      /// commit for each user
      await dbBatch.commit(noResult: true);
      dbBatch = db.batch();
    }
    var file = File(join(await getDatabasesPath(), _sqliteDBFileName));
    var fileLength = (await file.length())/1000000;
    debugPrint("DB SQLite file size: $fileLength Mb");
    outputFile.writeAsString("$index\t$fileLength\n",mode: FileMode.append);
    db.close();
    await deleteDatabase(join(await getDatabasesPath(), _sqliteDBFileName));
  });
  }



void main() {
  testCabspottingSQL();
}