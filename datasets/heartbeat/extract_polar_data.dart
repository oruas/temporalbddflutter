import 'dart:convert';
import 'dart:io';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

/// PPI data formatting
///
/// "PP interval (milliseconds) representing cardiac pulse-to-pulse interval
/// extracted from PPG signal."
/// https://github.com/polarofficial/polar-ble-sdk
///
void main(List<String> args) {
  // Check input arguments
  if (args.length != 1) {
    debugPrint('Expected format:\n\tdart path/to/extract_polar_data.dart path/to/polar_directory');
    exit(1);
  }

  // Check directory
  Directory dir = Directory(args[0]);
  if (!dir.existsSync()) {
    debugPrint('Input directory does not exist (was ${args[0]}).');
    exit(2);
  }

  // List files to be imported in the future dataset
  String expectedPrefix = 'ppi_samples_';
  List<String> filePaths = dir.listSync()
      .where((element) => element.statSync().type == FileSystemEntityType.file)
      .where((element) => element.path.split(Platform.pathSeparator).last.substring(0, expectedPrefix.length) == expectedPrefix)
      .toList()
      .map((e) => e.path)
      .sorted()
      .toList();

  // Create dataset file (recreate it if it already exists)
  File outputFile = File('datasets/heartbeat_pulse.txt');
  if (outputFile.existsSync()) {
    outputFile.deleteSync();
  }

  // Import file contents into dataset
  for (String filePath in filePaths) {
    // Check file existence
    File jsonFile = File(filePath);
    if (!jsonFile.existsSync()) {
      debugPrint('Polar file not found (input path was $filePath)');
      exit(3);
    }

    extractPpiDataToFile(jsonFile, outputFile);
  }

  debugPrint('Done.');
}

void extractPpiDataToFile(File source, File destination) {
  List<dynamic> data = json.decode(source.readAsStringSync());
  for (dynamic dayData in data) {
    assert(dayData['devicePpiSamplesList'].length, 1);
    dynamic dayPpiSamples = dayData['devicePpiSamplesList'][0]['ppiSamples'];
    debugPrint('=> ${dayData['date']} (${dayPpiSamples.length} samples)');
    for (dynamic sample in dayPpiSamples) {
      String formatted = '${DateTime.parse(sample['sampleDateTime']).millisecondsSinceEpoch};${sample['pulseLength']}\n';
      destination.writeAsStringSync(formatted, mode: FileMode.append);
    }
  }
}