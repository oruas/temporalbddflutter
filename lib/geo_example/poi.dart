library temporaldb;

import 'dart:collection';
import 'dart:math';

import 'package:tuple/tuple.dart';
import 'package:geolocator/geolocator.dart';

/// POI (Point Of Interest) attack.
///
/// Primault, V., Mokhtar, S. B., Lauradoux, C., & Brunie, L. (2014).
/// Differentially private location privacy in practice. arXiv preprint arXiv:1410.7744.
/// https://arxiv.org/pdf/1410.7744
///
/// Given GPS traces, the points of interest are the places where a user has
/// stayed a given amount of time. It reveals personal data such as work place
/// or home.
///
/// In this class, a GPS trace is represented as a List of Tuple (lat,lng)
/// representing the pair (latitude, longitude). The latitudes and longitudes
/// are expected to be in degrees

class POI {
  /// Performs the POI attack on a GPS trace to extract POIs.
  ///
  /// It first find areas, dubbed stays, which are places where the user has
  /// remained long enough in a small enough area; then, stays close to each
  /// other are merged into clusters.
  /// The centroids obtained from the clusters are the POIs of the input trace.
  ///
  /// The algorithm needs the following parameters (quoted from the paper):
  /// - [trace] is the list of GPS positions, each represented by a tuple
  /// (timestamp, lat, lng);
  /// - [minTime] is a time threshold (in seconds) representing the minimum time
  /// that must be spent in each stay;
  /// - [maxDistance] is a distance threshold (in meters) representing the
  /// maximal diameter of the stay area;
  /// - [minPts] is a minimum number of points necessary to form a cluster. This
  /// is the number of different points a cluster must feature to be maintained.
  static List<Tuple2<double, double>> getPOI(
      List<Tuple3<double, double, double>> trace,
      double minTime,
      double maxDistance,
      int minPts) {
    int nbPoints = trace.length;

    /// The list of stays.
    /// We keep the timestamps with the stays as we then store them in a set
    /// without the timestamp, two different (time-wise) stays could be seen
    /// as the same, and they would not be taken into account when checking
    /// if there are enough points ( # >= minPts ).
    List<Tuple3<double, double, double>> stays = [];

    /// The index of a stay candidate.
    /// A candidate is a sublist of trace where the points are at a distance
    /// lower than maxDistance.
    /// The algorithm is done 'in place': a candidate is represented by
    /// its starting index, indexStart and its ending index indexStop.
    /// indexStop is used as the general index to go through the whole list.
    int indexStart = 0;
    int indexStop = 0;
    double diameter;
    double timeDiff;
    while (indexStop < nbPoints) {
      diameter = 0;

      /// The diameter is the maximum distance between all the points of the
      /// candidate and the new points, i.e. the point at index indexStop.
      for (int i = indexStart; i < indexStop; i++) {
        diameter = max(
            diameter,
            Geolocator.distanceBetween(trace[i].item2, trace[i].item3,
                trace[indexStop].item2, trace[indexStop].item3));
      }

      /// If the diameter is still lower than the maximum distance, we only
      /// increase the size of the candidate.
      if (diameter <= maxDistance) {
        indexStop++;
      }

      /// Otherwise, the total time span of the candidate is evaluated.
      /// If the time is higher than minTime then the candidate is added as a
      /// stay; else, the first point is removed.
      else {
        timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
        if (timeDiff >= minTime) {
          stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
          indexStart = indexStop;
        } else {
          indexStart++;
        }
      }
    }

    /// End of the list, if there are still points in the current candidate.
    if (indexStop > indexStart) {
      timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
      if (timeDiff >= minTime) {
        stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
      }
    }

    /// The set of potential POIs.
    /// As stays contain multiple possible POIs, we need to make sure that stays
    /// does not contain several versions (POI visited several times): we merge
    /// the stays which are closed to each others.
    /// Those are merged into neighborhood; if a neighborhood is large enough,
    /// it is considered as a POI. The required frequency aims at avoiding
    /// "accidental" stays to be considered as POIs.
    HashSet<HashSet<Tuple3<double, double, double>>> clusters = HashSet();
    HashSet<Tuple3<double, double, double>> neighborhood;
    HashSet<HashSet<Tuple3<double, double, double>>> newClusters;

    /// We use `newClusters` as an auxiliary set to store the changes in clusters
    /// since we cannot change clusters while iterating over them (see doc).
    for (Tuple3<double, double, double> stay in stays) {
      /// We compute the neighborhood of each point.
      neighborhood = HashSet();

      /// We need a double loop (thus O(stays²) complexity) as each neighborhood
      /// is dependent on the current point; we can iterate on `stays` twice as
      /// it is not modified.
      for (Tuple3<double, double, double> stay_ in stays) {
        if (Geolocator.distanceBetween(
                stay.item2, stay.item3, stay_.item2, stay_.item3) <=
            0.75 * maxDistance) {
          neighborhood.add(stay_);
        }
      }

      /// If the current neighborhood is large enough, we keep it and see if it
      /// overlaps any other neighborhood stored in `clusters`.
      /// The overlapping are merged and the resulting cluster is stored.
      if (neighborhood.length >= minPts) {
        newClusters = HashSet();
        for (HashSet<Tuple3<double, double, double>> cluster in clusters) {
          if (intersectSets(cluster, neighborhood)) {
            for (Tuple3<double, double, double> centroid in cluster) {
              neighborhood.add(centroid);
            }
          } else {
            newClusters.add(cluster);
          }
        }
        // debugPrint('Size of newClusters: ${newClusters.length}');
        clusters = newClusters;
        clusters.add(neighborhood);
      }
    }

    /// We obtain the centroid of each cluster and return the list of results.
    /// Note that here, we obtain the centroid of centroids as it is performed
    /// in the original paper. For a more precise POI position, one might store
    /// the weight (i.e. number of points) of each centroid or the associated
    /// GPS points.
    List<Tuple2<double, double>> results = [];
    for (HashSet<Tuple3<double, double, double>> cluster in clusters) {
      results.add(getCentroid(cluster));
    }
    return results;
  }

  static bool intersectSets(Set set1, Set set2) {
    for (var item in set1) {
      if (set2.contains(item)) {
        return true;
      }
    }
    return false;
  }

  /// Computes the centroid on all the points of a given [trace].
  ///
  /// In this case, there is no timestamp: each point is (lat,lng)
  ///
  /// ***** WARNING *****
  /// Currently, the centroid is obtained by computing the average.
  /// It is efficient enough since the POI is not the end purpose of the
  /// library. For more precise application, one might want an improved version.
  /// *******************
  static Tuple2<double, double> getCentroid(
      Iterable<Tuple3<double, double, double>> trace) {
    double lat = 0;
    double lng = 0;

    for (var point in trace) {
      lat += point.item2;
      lng += point.item3;
    }
    lat = lat / trace.length;
    lng = lng / trace.length;

    return Tuple2(lat, lng);
  }

  /// Computes the centroid of the points within a certain range in a given GPS
  /// [trace].
  ///
  /// In the [trace], each point follows the format (timestamp, lat, lng).
  static Tuple3<double, double, double> getCentroidFromIndex(
      List<Tuple3<double, double, double>> trace,
      int indexStart,
      int indexStop) {
    double lat = 0;
    double lng = 0;

    for (int i = indexStart; i <= indexStop; i++) {
      lat += trace[i].item2;
      lng += trace[i].item3;
    }
    lat = lat / (indexStop - indexStart + 1);
    lng = lng / (indexStop - indexStart + 1);

    return Tuple3(trace[indexStart].item1, lat, lng);
  }

  /// Computes the stays regularly.
  static List<Tuple3<double, double, double>> getStays(
      List<Tuple3<double, double, double>> trace,
      double minTime,
      double maxDistance) {
    int nbPoints = trace.length;

    /// The list of stays.
    /// We keep the timestamp with the stay as we then store them in a set
    /// without the timestamp, two different (time-wise) stays could be seen
    /// as the same, and they would not be taken into account when checking
    /// if there are enough points ( # >= minPts ).
    List<Tuple3<double, double, double>> stays = [];

    /// A candidate is a sublist of trace where the points are at a distance
    /// lower than `maxDistance`.
    /// The algorithm is done 'in place': a candidate is represented by
    /// its starting index `indexStart` and its ending index `indexStop`.
    /// `indexStop` is used as the general index to go through the whole list.
    int indexStart = 0;
    int indexStop = 0;
    double diameter;
    double timeDiff;
    while (indexStop < nbPoints) {
      diameter = 0;

      /// The diameter is the maximum distance between all the points of the
      /// candidate and the new points, i.e. the point at index `indexStop`.
      for (int i = indexStart; i < indexStop; i++) {
        diameter = max(
            diameter,
            Geolocator.distanceBetween(trace[i].item2, trace[i].item3,
                trace[indexStop].item2, trace[indexStop].item3));
      }

      /// If the diameter is still lower than the maximum distance, we only
      /// increase the size of the candidate.
      if (diameter <= maxDistance) {
        indexStop++;
      }

      /// Otherwise, the total time span of the candidate is evaluated.
      /// If the time is higher than `minTime`, then the candidate is added as a
      /// stay; else, the first point is removed.
      else {
        timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
        if (timeDiff >= minTime) {
          stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
          indexStart = indexStop;
        } else {
          indexStart++;
        }
      }
    }

    /// End of the list, if there are still points in the current candidate.
    if (indexStop > indexStart) {
      timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
      if (timeDiff >= minTime) {
        stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
      }
    }
    return stays;
  }

  /// Computes the stays regularly on a specific part of a [trace].
  ///
  /// In particular, it does not stop at the stopIndex if the associated GPS
  /// point is in a potential stay, it stops after either dismissing this
  /// possibility or after computing the associated stay.
  static List<Tuple3<double, double, double>> _getStaysSubTrace(
      List<Tuple3<double, double, double>> trace,
      int indexStart_,
      int indexStop_,
      double minTime,
      double maxDistance) {
    int nbPoints = trace.length;

    /// The list of stays.
    /// We keep the timestamp with the stay as we then store them in a set
    /// without the timestamp, two different (time-wise) stays could be seen as
    /// the same, and they would no be taken into account when checking if there
    /// are enough points ( # >= minPts ).
    List<Tuple3<double, double, double>> stays = [];

    /// A candidate is a sublist of trace where the points are at a distance
    /// lower than maxDistance.
    /// The algorithm is done 'in place': a candidate is represented by
    /// its starting index `indexStart` and its ending index `indexStop`.
    /// `indexStop` is used as the general index to go through the whole list.
    int indexStart = indexStart_;
    int indexStop = 0;
    double diameter;
    double timeDiff;
    while (indexStop < nbPoints && indexStart <= indexStop_) {
      diameter = 0;

      /// The diameter is the maximum distance between all the points of the
      /// candidate and the new points, i.e. the point at index `indexStop`.
      for (int i = indexStart; i < indexStop; i++) {
        diameter = max(
            diameter,
            Geolocator.distanceBetween(trace[i].item2, trace[i].item3,
                trace[indexStop].item2, trace[indexStop].item3));
      }

      /// If the diameter is still lower than the maximum distance, we only
      /// increase the size of the candidate.
      if (diameter <= maxDistance) {
        indexStop++;
      }

      /// Otherwise, the total time span of the candidate is evaluated.
      /// If the time is higher than `minTime`, then the candidate is added as a
      /// stay; else, the first point is removed.
      else {
        timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
        if (timeDiff >= minTime) {
          stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
          indexStart = indexStop;
        } else {
          indexStart++;
        }
      }
    }

    /// End of the list, if there are still points in the current candidate.
    if (indexStop > indexStart) {
      timeDiff = (trace[indexStop - 1].item1 - trace[indexStart].item1).abs();
      if (timeDiff >= minTime) {
        stays.add(getCentroidFromIndex(trace, indexStart, indexStop - 1));
      }
    }
    return stays;
  }

  /// Finds the stays of a [trace] with a top-down approach.
  ///
  /// Instead of considering the [trace] iteratively, point by point, it
  /// considers it as a whole. The trace is divided in two parts, cut in the
  /// middle; Both segments, left and right, are individually considered.
  ///
  /// If the start and endpoint of the segment are close temporally but far
  /// spatially, it means that no POI is possible: we do not compute POI on this
  /// segment. Otherwise, we recursively compute POIs with the top-down approach
  /// on the segment until the size is lower than a given number, e.g. 300.
  ///
  /// In that case, we use the classical POI attack on the considered sublist.
  /// In practice, we manipulate the same list but with different indexes
  /// for the recursion.
  ///
  /// We may miss stays around the middle points, but we chose to ignore them
  /// as a POI is a cluster of several stays: it is very unlikely to miss them
  /// all.
  static List<Tuple3<double, double, double>> _getTopDownStays(
      List<Tuple3<double, double, double>> trace,
      int startIndex,
      int stopIndex,
      double minTime,
      double maxDistance) {
    const int sizeMinimalWindow = 300;
    int traceLength = stopIndex - startIndex;

    if (traceLength <= 1) {
      return [];
    }
    if (traceLength <= sizeMinimalWindow) {
      return getStays(
          trace.sublist(startIndex, stopIndex), minTime, maxDistance);
    }

    List<Tuple3<double, double, double>> stays = [];
    int indexMiddle = startIndex + traceLength ~/ 2;

    /// *********** First half **********
    /// We compute the stays on the first half of the trace.
    double dStartMiddle = Geolocator.distanceBetween(
        trace[startIndex].item2,
        trace[startIndex].item3,
        trace[indexMiddle].item2,
        trace[indexMiddle].item3);
    double tStartMiddle =
        (trace[indexMiddle].item1 - trace[startIndex].item1).abs();

    /// If points are too close in time while far from each other:
    /// -> no possible POI.
    /// Otherwise, we check POIs recursively in the left part.
    if (!(dStartMiddle > maxDistance && tStartMiddle <= minTime)) {
      stays += _getTopDownStays(
          trace, startIndex, indexMiddle, minTime, maxDistance);
    }

    /// *********** Second half **********
    /// We compute the stays on the second half of the trace.
    double dEndMiddle = Geolocator.distanceBetween(
        trace[indexMiddle].item2,
        trace[indexMiddle].item3,
        trace[stopIndex].item2,
        trace[stopIndex].item3);
    double tEndMiddle =
        (trace[stopIndex].item1 - trace[indexMiddle].item1).abs();

    /// Same as before: if points are too close in time while far from each
    /// other: -> no possible POI.
    /// Otherwise we check the POIs recursively in the right part.
    if (!(dEndMiddle > maxDistance && tEndMiddle <= minTime)) {
      stays +=
          _getTopDownStays(trace, indexMiddle, stopIndex, minTime, maxDistance);
    }
    return stays;
  }

  /// Computes the regular POI attack with a top-down approach.
  ///
  /// The only difference with the regular approach is how the stays are
  /// computed, relying on a top-down approach instead of the iterative one.
  static List<Tuple2<double, double>> getTopDownPOI(
      List<Tuple3<double, double, double>> trace,
      double minTime,
      double maxDistance,
      int minPts) {
    /// Stays are found with a topDown approach.
    List<Tuple3<double, double, double>> stays =
        _getTopDownStays(trace, 0, trace.length - 1, minTime, maxDistance);

    HashSet<HashSet<Tuple3<double, double, double>>> clusters = HashSet();
    HashSet<Tuple3<double, double, double>> neighborhood;
    HashSet<HashSet<Tuple3<double, double, double>>> newClusters;

    /// We use `newClusters` as an auxiliary set to store the changes in clusters
    /// since we cannot change `clusters` while iterating over it (see doc).

    for (Tuple3<double, double, double> stay in stays) {
      /// We compute the neighborhood of each point.
      neighborhood = HashSet();

      /// We need a double loop (thus O(stays²) complexity) as each neighborhood
      /// is dependent on the current point.
      /// We can iterate on `stay` twice as it is not modified.
      for (Tuple3<double, double, double> stay_ in stays) {
        if (Geolocator.distanceBetween(
                stay.item2, stay.item3, stay_.item2, stay_.item3) <=
            0.75 * maxDistance) {
          neighborhood.add(stay_);
        }
      }

      /// If the current neighborhood is large enough, we keep it and see if it
      /// overlaps any other neighborhood stored in `clusters`.
      /// The overlapping are merged and the resulting cluster is stored.
      if (neighborhood.length >= minPts) {
        newClusters = HashSet();
        for (HashSet<Tuple3<double, double, double>> cluster in clusters) {
          if (intersectSets(cluster, neighborhood)) {
            for (Tuple3<double, double, double> centroid in cluster) {
              neighborhood.add(centroid);
            }
          } else {
            newClusters.add(cluster);
          }
        }
        clusters = newClusters;
        clusters.add(neighborhood);
      }
    }

    /// We obtain the centroid of each cluster and return the list of results.
    /// Note that here, we obtain the centroid of centroids as it is performed
    /// in the original paper. For a more precise POI position, one might store
    /// the weight (i.e. number of points) of each centroid or the associated
    /// GPS points.
    List<Tuple2<double, double>> results = [];
    for (HashSet<Tuple3<double, double, double>> cluster in clusters) {
      results.add(getCentroid(cluster));
    }
    return results;
  }
}
