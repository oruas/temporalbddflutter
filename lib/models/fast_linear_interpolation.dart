library temporaldb;

import 'package:flutter/material.dart';
import 'package:scidart/numdart.dart';
import 'package:tuple/tuple.dart';

import 'temporal_model.dart';

/// Fast Linear Interpolation (FLI) aims at modeling a time series with
/// successive linear interpolations.
///
/// We model successive points by a straight line between the first and the last
/// points. When adding a new point, it becomes the last point of the
/// interpolation. We need to check that the new model does not incur an error
/// superior to a threshold to the old points.
///
/// In order to improve the performances, we make several choices:
///
/// 1) We change the axis such as the first point (t,v) of the current model is
/// the origin: adding a point (t2,v2) consists in adding (t2-t,v2-v); this way,
/// the straight line is a polynomial of the form v = ax (no intercept);
///
/// 2) Instead of computing the new error on each point in the list, we use two
/// parameters: the maximum and minimum acceptable values for the parameter A.
/// When adding a point (t',v'), it is easy to compute the maximum and minimum
/// value for A such as the estimate At' does not produce an error superior
/// to a fixed value E: we have v'+E = maxA * t'
/// So maxA = (v'+E)/t'
/// Similarly: minA = (v'-E)/t'
///
/// Since we do not want an error for any of the already inserted points (in the
/// current model), we need the new A to be within those [minA; maxA] bounds for
/// all the previous points (except the first and last as we do an interpolation).
///
/// So we only need to keep two variable minA and maxA and when a new point is
/// added, we compute the new A, see if it is within [minA;maxA] and update
/// those two values:
///     minA = max(newMinA,minA)
///     maxA = min(newMaxA,maxA)
/// Those two values should converge if there is no error triggering a change in
/// model. Thanks to them, we do not need to keep the list of inserted points.

/// The default value for the error is 0.01
class FastLinearInterpolation implements TemporalModel {
  /// The parameters (for time and value) are either raw or normalized.
  /// Their status is specified at the end of their name: `timeRaw` is a raw
  /// param, while `timeNorm` is normalized.
  /// Here, normalized stands for the fact that tRaw, vRaw are delayed by some
  /// values t0, v0 => tNorm = tRaw-t0 and vNorm = vRaw - v0.
  /// Normalized values are used to compute the parameters A and estimating
  /// values, while raw values are stored as initial points of models.

  /// The starting time of the global model.
  double firstStartingTimeRaw = -1;

  /// The list of old models.
  /// Each model is represented by a tuple (t, v, a), where `t` is the starting
  /// time, `v` is the associated value, also used as intercept, and `a` is the
  /// polynomial, i.e. v_ = a(t_-t) + v.
  List<Tuple3<double, double, double>> listOldModels = [];

  /// Variables for the current linear interpolation.
  double currentStartingTimeRaw = 0;

  /// The starting time of the current model.
  double currentStartingValueRaw = 0;

  /// The intercept of the current model.
  double currentA = 0;

  /// Polynomial of the current model:
  /// v' = currentA * (t'-currentTime)
  double maxPossibleA = double.maxFinite;
  double minPossibleA = -double.maxFinite;

  /// The last inserted point, used to start the new model.
  double lastTimeRaw = 0;
  double lastValueRaw = 0;

  /// The maximum tolerated error for the whole model.
  double maxError = 0.01;

  @override
  void add(double tRaw, double vRaw) {
    if (firstStartingTimeRaw == -1) {
      firstStartingTimeRaw = tRaw;
      _initModel(tRaw, vRaw);
      return;
    }

    double tNorm = tRaw - currentStartingTimeRaw;
    double vNorm = vRaw - currentStartingValueRaw;
    double A = vNorm / tNorm;

    /// If the points fits, we update the values and return.
    if (A <= maxPossibleA && A >= minPossibleA) {
      minPossibleA = max(minPossibleA, (vNorm - maxError) / tNorm);
      maxPossibleA = min(maxPossibleA, (vNorm + maxError) / tNorm);
      lastTimeRaw = tRaw;
      lastValueRaw = vRaw;
      currentA = A;
      return;
    }

    /// If the point does not fit, it means that the new interpolation would end
    /// up raising an error on at least one of the previous points: we keep the
    /// current interpolation and start a new model.
    listOldModels
        .add(Tuple3(currentStartingTimeRaw, currentStartingValueRaw, currentA));

    /// The new interpolation starts with the last point of the previous model.
    currentStartingTimeRaw = lastTimeRaw;
    currentStartingValueRaw = lastValueRaw;
    tNorm = tRaw - lastTimeRaw;
    vNorm = vRaw - lastValueRaw;
    A = vNorm / tNorm;

    /// In this case, the error is 0 for all the points, so we update the model.
    minPossibleA = (vNorm - maxError) / tNorm;

    /// There are no other points, so no max.
    maxPossibleA = (vNorm + maxError) / tNorm;

    /// Idem.
    lastTimeRaw = tRaw;
    lastValueRaw = vRaw;
    currentA = A;
    return;
  }

  void _initModel(double tRaw, double vRaw) {
    currentStartingTimeRaw = tRaw;
    currentStartingValueRaw = vRaw;
    currentA = 0;
    maxPossibleA = double.maxFinite;
    minPossibleA = -double.maxFinite;
    lastTimeRaw = tRaw;
    lastValueRaw = vRaw;
  }

  @override
  List<List<double>> getModels() {
    List<List<double>> models = [];
    for (var tuple3 in listOldModels) {
      models.add([tuple3.item3, tuple3.item2]);
    }
    models.add([currentA, currentStartingValueRaw]);
    return models;
  }

  @override
  int getNbModels() {
    return (listOldModels.length + 1);
  }

  @override
  int getSize() {
    return (9 + 3 * listOldModels.length);
  }

  @override
  List<double> getTimestamps() {
    List<double> timestamps = [];
    for (var tuple3 in listOldModels) {
      timestamps.add(tuple3.item1);
    }
    timestamps.add(currentStartingTimeRaw);
    return timestamps;
  }

  @override
  List<Tuple2<double, List<double>>> getModelsAndTimestamps() {
    List<Tuple2<double, List<double>>> modelsAndTimestamps = [];
    for (var tuple3 in listOldModels) {
      modelsAndTimestamps
          .add(Tuple2(tuple3.item1, [tuple3.item3, tuple3.item2]));
    }
    return modelsAndTimestamps;
  }

  List<Tuple2<double, double>> get data {
    return [
      ...listOldModels.map((e) => Tuple2(e.item1, e.item2)).toList(),
      Tuple2(lastTimeRaw, lastValueRaw)
    ];
  }

  @override
  String getType() {
    return "Fast Linear Interpolation";
  }

  @override
  double read(double t) {
    /// If the reading time is before the time of the first model, we throw.
    if (firstStartingTimeRaw == -1 || t < firstStartingTimeRaw) {
      throw ErrorDescription("Error the time is unavailable");
    }
    if (currentStartingTimeRaw <= t) {
      return _getValue(
          t, currentStartingTimeRaw, currentA, currentStartingValueRaw);
    }
    // for(var tuple3 in listOldModels.toList().reversed) {
    //   if (tuple3.item1 <= t) {
    //     return _getValue(t, tuple3.item1, tuple3.item3, tuple3.item2);
    //   }
    // }
    if (listOldModels.isEmpty) {
      throw ErrorDescription("Error the time is unavailable (!)");
    }
    /*Tuple3<double,double,double> model=const Tuple3(0,0,0);
    // int j=0;
    for(var currentModel in listOldModels) {
    // for(int i=1; i < listOldModels.length-1; i++) {
    //   model = listOldModels[i];
      /// As long as the model has a timestamp lower, we move to the next one
      /// Once we found a model created later than than t, we stop
      if (currentModel.item1 > t) {
        // j = i-1;
        break;
      }
      model = currentModel;
    }*/
    // model = listOldModels[j];
    int index = _getIndexRead(t);
    Tuple3<double, double, double> model = listOldModels[index];
    return _getValue(t, model.item1, model.item3, model.item2);

    // /// This code should be unreachable thanks to the first if
    // throw ErrorDescription("Error the time is unavailable (!)");
  }

  int _getIndexRead(double t) {
    int minIndex = 0;
    int maxIndex = listOldModels.length - 1;
    int index = -1;
    double tIndex;
    while (minIndex <= maxIndex) {
      index = (maxIndex + minIndex) ~/ 2;
      if (minIndex == maxIndex) {
        break;
      }
      tIndex = listOldModels[index].item1;
      if (t == tIndex) {
        break;
      }
      // debugPrint('$index $minIndex $maxIndex ${listOldModels.length}');
      if (t > tIndex && t < listOldModels[index + 1].item1) {
        break;
      }
      if (t < tIndex) {
        maxIndex = index - 1;
      } else {
        minIndex = index + 1;
      }
    }
    return index;
  }

  /// Returns the estimated value for t by the polynomial Ax+B, centered in T,
  /// i.e. A(t-T)+B.
  static double _getValue(double t, double T, double A, double B) {
    return (A * (t - T) + B);
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "error":
          {
            maxError = double.parse(value);
          }
          break;
      }
    });
  }

  @override
  String toString() {
    String s = "Fast Linear Interpolation:\n";
    s += "[";
    for (var tuple3 in listOldModels) {
      s += "\t" +
          tuple3.item1.toString() +
          ": " +
          tuple3.item3.toString() +
          " " +
          tuple3.item2.toString() +
          "\n";
    }
    s += "\t" +
        currentStartingTimeRaw.toString() +
        ": " +
        currentA.toString() +
        " " +
        currentStartingValueRaw.toString() +
        "\n";
    s += "]";
    return s;
  }
}
