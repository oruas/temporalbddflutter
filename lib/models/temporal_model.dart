library temporaldb;

import 'package:tuple/tuple.dart';

/// [TemporalModel] is an abstract class representing temporal models.
///
/// A temporal model is a unidimensional model used to model double values
/// in time. You can add and read values. The model is a piece-wise model so
/// it is composed of several smaller models.
///
/// Note that points have to be added in chronological order.
abstract class TemporalModel {
  /// Adds a value to the model, associated to a timestamp [t_].
  ///
  /// Throws an exception if the timestamp is not accessible in the model. Note
  /// that points have to be added in chronological order.
  void add(double t_, double v_);

  /// Returns the estimation of the value by the model at the timestamp [t].
  ///
  /// Throws an exception if the timestamp is not accessible in the model.
  double read(double t);

  /// Returns the size (in terms of number of parameters, e.g. double/int) of
  /// the model
  int getSize();

  /// Returns the number of models used as we are working with piece-wise models.
  int getNbModels();

  /// Returns the models.
  List<List<double>> getModels();

  /// Returns the timestamps of the models.
  List<double> getTimestamps();

  /// Sets the parameters of the model.
  void setParameters(Map<String, String> map);

  /// Returns the type of the model.
  String getType();

  /// Returns the models and their associated timestamps.
  List<Tuple2<double, List<double>>> getModelsAndTimestamps();
}
