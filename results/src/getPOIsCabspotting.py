#!/usr/bin/env python3

import folium as fo
import json

user = 0
inputAdd = './POI-cabspotting-user-'+str(user)+'/'
outputAdd = './POI-cabspotting-user-'+str(user)+'/'

addPOIRaw = inputAdd+'poiRaw.txt'
addPOIRawTopDown = inputAdd+'poiRawTopDown.txt'
addPOIRawPromesse = inputAdd+'poiRawPromesse.txt'
addPOIRawPromesseTopDown = inputAdd+'poiRawPromesseTopDown.txt'

addPOIModeled = inputAdd+'poiModeled.txt'
addPOIModeledTopDown = inputAdd+'poiModeledTopDown.txt'
addPOIModeledPromesse = inputAdd+'poiModeledPromesse.txt'
addPOIModeledPromesseTopDown = inputAdd+'poiModeledPromesseTopDown.txt'


def readData(filename):
    rawContent = open(filename,'r').read()
    result = json.loads(rawContent)
    return result
    



def printTwoTraces(data1,data2,outputName):
    dataCommon = [l for l in data1 if l in data2]
    datas = [data1, data2]
    colors = ['blue','green']
    volcano = fo.FeatureGroup(name="Volcano")
    for data, color in zip (datas, colors):
        for lat,lng in data:
            l=[lat,lng]
            volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color=color)))
    for l in dataCommon:
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='red')))
    m = fo.Map().add_child(volcano)
    m.save(outputName+'-same-'+str(len(dataCommon))+'.html')

def printTraces(data1,data2,data3,data4,outputName):
    datas = [data1, data2, data3, data4]
    colors = ['blue','lightblue','green','lightgreen']
    volcano = fo.FeatureGroup(name="Volcano")
    for data, color in zip (datas, colors):
        for lat,lng in data:
            l=[lat,lng]
            volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color=color)))
    m = fo.Map().add_child(volcano)
    m.save(outputName)


def printTraceLimit(limit):
    data = readData('trace.txt')
    dataPromesse = readData('tracePromesse.txt')
    volcano = fo.FeatureGroup(name="Volcano")
    nb=0
    for t,lat,lng in data:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, marker=t, icon=fo.Icon(color='blue')))
        nb=nb+1
        if (nb>limit):
            break
    nb=0
    for t,lat,lng in dataPromesse:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, marker=t, icon=fo.Icon(color='green')))
        nb=nb+1
        if (nb>limit/2):
            break
    m = fo.Map().add_child(volcano)
    m.save('traceLimit.html')

def main():
    dataRaw = readData(addPOIRaw)
    dataRawTopDown = readData(addPOIRawTopDown)
    dataModeled = readData(addPOIModeled)
    dataModeledTopDown = readData(addPOIModeledTopDown)
    printTraces(dataRaw,dataRawTopDown,dataModeled,dataModeledTopDown, outputAdd+'allPOICabspottingUser0.html')
    printTwoTraces(dataRaw,dataRawTopDown,outputAdd+'poi_raw_topdow')
    printTwoTraces(dataRaw,dataModeled,outputAdd+'poi_raw_modeled')
    printTwoTraces(dataModeled,dataModeledTopDown,outputAdd+'poi_modeled_TopDown')
    printTwoTraces(dataRaw,dataModeledTopDown,outputAdd+'poi_raw_modeledTopDown')


main()
