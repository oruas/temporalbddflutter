#!/usr/bin/env python3

import folium as fo
import json


def readData(filename):
    rawContent = open(filename,'r').read()
    result = json.loads(rawContent)
    return result
    


def printTrace():
    data = readData('trace.txt')
    dataPromesse = readData('tracePromesse.txt')
    volcano = fo.FeatureGroup(name="Volcano")
    for t,lat,lng in data:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='blue')))
    for t,lat,lng in dataPromesse:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='green')))
    m = fo.Map().add_child(volcano)
    m.save('trace.html')



def printTraceLimit(limit):
    data = readData('trace.txt')
    dataPromesse = readData('tracePromesse.txt')
    volcano = fo.FeatureGroup(name="Volcano")
    nb=0
    for t,lat,lng in data:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, marker=t, icon=fo.Icon(color='blue')))
        nb=nb+1
        if (nb>limit):
            break
    nb=0
    for t,lat,lng in dataPromesse:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, marker=t, icon=fo.Icon(color='green')))
        nb=nb+1
        if (nb>limit/2):
            break
    m = fo.Map().add_child(volcano)
    m.save('traceLimit.html')

def printPOI():
    # data = readData('privamovPOIRegular.txt')
    # dataPromesse = readData('privamovPOITopDown.txt')
    data_raw = readData('privamovPOIRegularPrivamov1Raw.txt')
    dataPromesse_raw = readData('privamovPOITopDownPrivamov1Raw.txt')
    data = [[lat,lng] for [lng,lat] in data_raw]
    dataPromesse = [[lat,lng] for [lng,lat] in dataPromesse_raw]
    dataCommon = [l for l in data if l in dataPromesse]
    volcano = fo.FeatureGroup(name="Volcano")
    for l in data:
        if (not l in dataCommon):
            volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='blue')))
    for l in dataPromesse:
        if (not l in dataCommon):
            volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='green')))
    for l in dataCommon:
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='red')))
    m = fo.Map().add_child(volcano)
    m.save('POIPrivamov1Raw.html')

    
def printModeledTrace():
    data = readData('trace10.txt')
    modeledData = readData('modeledTrace10.txt')
    volcano = fo.FeatureGroup(name="Volcano")
    for t,lat,lng in data:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='blue')))
    for t,lat,lng in modeledData:
        l=[lat,lng]
        volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='green')))
    m = fo.Map().add_child(volcano)
    m.save('modeledTrace10.html')



def main():
    # printModeledTrace()
    # printTraceLimit(500)
    # printTrace()
    printPOI()
    # data = readData('trace.txt')
    # print(data)
    # volcano = fo.FeatureGroup(name="Volcano")
    # for l in data:
    #     volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='blue')))
    # for l in dataPromesse:
    #     volcano.add_child(fo.Marker(location=l, icon=fo.Icon(color='red')))

  
    # m = fo.Map().add_child(volcano)
    # m.save('essai.html')

main()
