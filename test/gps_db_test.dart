import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/poi.dart';
import 'package:temporaldb/geo_example/promesse.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/gps_db.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';
import 'package:collection/collection.dart';


/// Testing GPSDB on the artificial dataset
void testStoreCSVArtificialDataset() {
  test('storing the Artificial Dataset', ()
  {
    var dir = Directory.current.path+'/datasets/';

    List<Tuple3<double,double,double>> gpsTraceUser0 = readCSVUser(dir+'artificial_dataset.txt',0);
    GPSDB modeledTrace0 = GPSDB();
    List<double> timestamps=[];
    for(var tuple3 in gpsTraceUser0) {
      modeledTrace0.add(tuple3.item1, tuple3.item2, tuple3.item3);
      timestamps.add(tuple3.item1);
    }
    List<Tuple3<double,double,double>> trace0 = modeledTrace0.getTrace(timestamps);

    List<Tuple3<double,double,double>> gpsTraceUser1 = readCSVUser(dir+'artificial_dataset.txt',1);
    GPSDB modeledTrace1 = GPSDB();
    timestamps=[];
    for(var tuple3 in gpsTraceUser1) {
      modeledTrace1.add(tuple3.item1, tuple3.item2, tuple3.item3);
      timestamps.add(tuple3.item1);
    }
    List<Tuple3<double,double,double>> trace1 = modeledTrace1.getTrace(timestamps);


    Function eq = const ListEquality().equals;

    bool listEq0 = eq(trace0,gpsTraceUser0);
    bool listEq1 = eq(trace1,gpsTraceUser1);
    bool listEq = listEq0 && listEq1;

    // debugPrint('Size of raw trace 0: ${(3*gpsTraceUser0.length).toString()}');
    // debugPrint('Size of final model 0: ${modeledTrace0.getSize().toString()}');
    // debugPrint('Size of raw trace 1: ${(3*gpsTraceUser1.length).toString()}');
    // debugPrint('Size of final model 1: ${modeledTrace1.getSize().toString()}');
    expect(listEq,true);
  });
}
/// GPSDB(5,{'error':'0.001'}); -> 5 used to be SWAB
/// Size of traces:
/// Raw data: 1306989
/// Model: 29256
/// Testing the POI attack and Promesse LPPM on a single user
void testPromesse() {
  test('testing the POI attack and Promesse LPPM on a single user', () {
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    List<Tuple3<double,double,double>> gpsTraceUser_ = readCSVUser(dir+'cabspotting',0);
    // List<Tuple3<double,double,double>> gpsTraceUser_ = readCSVUser(dir+'privamov-gps-user1-sampling10',1);

    List<Tuple3<double,double,double>> gpsTraceUser = [];
    GPSDB gpsModel = GPSDB(0,{'error':'0.001'});
    List<double> timestamps = [];

    List<double> distances = [];
    // for(var point in gpsTraceUser_) { /// not reversed for privamov
    for(var point in gpsTraceUser_.reversed) { /// reversed for cabspotting
      gpsTraceUser.add(point);
      gpsModel.add(point.item1, point.item2, point.item3);
      timestamps.add(point.item1);
    }
    var modeledTrace = gpsModel.getTrace(timestamps);

    var poiRaw = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> gpsTraceUserPromesse = Promesse.smoothSpeed(gpsTraceUser, epsilon);
    var poiPromesseRaw = POI.getPOI(gpsTraceUserPromesse, minTime, minDistance, minPts);

    var poiModeled = POI.getPOI(modeledTrace, minTime, minDistance, minPts);
    List<Tuple3<double,double,double>> modeledTracePromesse = Promesse.smoothSpeed(modeledTrace, epsilon);
    var poiPromesseModeled = POI.getPOI(modeledTracePromesse, minTime, minDistance, minPts);


    // writeResult(Directory.current.path+'/results/trace10.txt', gpsTraceUser.sublist(0,10).toString());
    // writeResult(Directory.current.path+'/results/modeledTrace10.txt', modeledTrace.sublist(0,10).toString());
    // writeResult(Directory.current.path+'/results/resultsPOI.txt', poi.toString());
    // writeResult(Directory.current.path+'/results/resultsPOIPromesse.txt', poiPromesse.toString());


    debugPrint('Number of points:');
    debugPrint('Raw data: ${gpsTraceUser.length}');
    debugPrint('Model: ${modeledTrace.length}');
    debugPrint('Size of traces:');
    debugPrint('Raw data: ${gpsTraceUser.length*3}');
    debugPrint('Model: ${gpsModel.getSize()}');
    debugPrint('Number of POI of unprotected trace:');
    debugPrint('Raw data: ${poiRaw.length}');
    debugPrint('Modeled data: ${poiModeled.length}');
    debugPrint('Number of POI with Promesse:');
    debugPrint('Raw data: ${poiPromesseRaw.length}');
    debugPrint('Modeled data: ${poiPromesseModeled.length}');
    bool b = (poiRaw.length == poiModeled.length && poiPromesseModeled.length == poiPromesseRaw.length);


    distances += distributionDistancePOI(poiModeled,poiRaw);
    distances.sort();
    debugPrint(distances.toString());
    List<Tuple2<double,double>> cumulDistrib = cumulativeDistribution(distances);
    debugPrint(cumulDistrib.toString());

    expect(b,true);
  });
}

/// Testing the POI attack and the Promesse LPPM on every user of the Cabspotting
/// dataset. We check that the protected trace has fewer POI than the raw one.
void testPromesseCabspotting() {
  test('testing POI/Promesse on the Cabspotting dataset', () {
    double minTime = 60*5;
    double minDistance = 500;
    double epsilon = 1000;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    Map<int,List<Tuple3<double,double,double>>> gpsDataset = readCSV(dir+'cabspotting');
    bool b = true;
    List<double> timestamps;
    double totalRaw = 0;
    double totalModeled = 0;
    double rawSize;
    double modelSize;
    List<double> distances = [];
    for(var entry in gpsDataset.entries) {
      List<Tuple3<double, double, double>> gpsTraceUser = [];
      timestamps=[];
      GPSDB gpsModel = GPSDB();
      for (var point in entry.value.reversed) {
        gpsTraceUser.add(point);
        gpsModel.add(point.item1, point.item2, point.item3);
        timestamps.add(point.item1);
      }
      var modeledTrace = gpsModel.getTrace(timestamps);

      var poiRaw = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
      List<Tuple3<double, double, double>> gpsTraceUserPromesse = Promesse
          .smoothSpeed(gpsTraceUser, epsilon);
      var poiPromesseRaw = POI.getPOI(
          gpsTraceUserPromesse, minTime, minDistance, minPts);

      var poiModeled = POI.getPOI(modeledTrace, minTime, minDistance, minPts);
      List<Tuple3<double, double, double>> modeledTracePromesse = Promesse
          .smoothSpeed(modeledTrace, epsilon);
      var poiPromesseModeled = POI.getPOI(
          modeledTracePromesse, minTime, minDistance, minPts);

      rawSize = gpsTraceUser.length * 3;
      modelSize = gpsModel.getSize().toDouble();

      distances += distributionDistancePOI(poiModeled,poiRaw);

      debugPrint('******** user ${entry.key} ********');
      debugPrint('Number of points:');
      debugPrint('Raw data: ${gpsTraceUser.length}');
      debugPrint('Model: ${modeledTrace.length}');
      debugPrint('Size of traces:');
      debugPrint('Raw data: $rawSize');
      debugPrint('Model: $modelSize');
      debugPrint('Gain: ${(100*(rawSize-modelSize))/rawSize}%');
      debugPrint('Number of POI of unprotected trace:');
      debugPrint('Raw data: ${poiRaw.length}');
      debugPrint('Modeled data: ${poiModeled.length}');
      debugPrint('Number of POI with Promesse:');
      debugPrint('Raw data: ${poiPromesseRaw.length}');
      debugPrint('Modeled data: ${poiPromesseModeled.length}');
      totalRaw += rawSize;
      totalModeled += modelSize;
      b = b && (poiRaw.length == poiModeled.length &&
          poiPromesseModeled.length == poiPromesseRaw.length);
    }
    List<Tuple2<double,double>> cumulDistrib = cumulativeDistribution(distances);
    writeResult(Directory.current.path+'/results/distribErrorPOICabspotting.txt',cumulDistrib.toString());
    debugPrint('Total gain: ${(100*(totalRaw-totalModeled))/totalRaw}%');
    expect(b, true);
  });
}

void testREAD() {
  test('testing dichotomy read', ()
  {
  TemporalModel model = Factory(0,{'error':'0.001'});
  for(int i=0; i<100; i++) {
    model.add(i.toDouble(),i/2 % 2);
  }
  double t;
  double v;
  debugPrint('nb de models: ${model.getNbModels()}');
  for(int i=0; i<100; i++) {
    t = i.toDouble();
    v = model.read(t);
    debugPrint('$t $v');
  }
  // model.add(0,0);
  // model.add(1,1);
  // model.add(2,1);
  // model.add(3,0);
  // debugPrint(model.read(0.5));
  // debugPrint(model.read(0.5));
  expect(true, true);
  });
}

void testTimestampModeling() {
  test('testing modeling timestamps on the Cabspotting dataset user 0', ()
  {
    /// Results for privamov-gps-user1 using an error of 1:
    /// Number of points: 4341716.0
    /// Number of models: 26862
    /// Size of model: 80592.0
    /// Gain: 98.14377541045982%
    /// mae: 0.246398716079986


    var dir = Directory.current.path + '/datasets/';
    List<Tuple3<double, double, double>> gpsTraceUser_ = readCSVUser(
        // dir + 'privamov-gps-user1-sampling10', 1);
        dir + 'privamov-gps-user1', 1);
        // dir + 'cabspotting', 1);
    List<double> timestampsUser = [];
    TemporalModel model = Factory(0, const {'error': '1'});
    int timestampIndex = 0;
    for (var point in gpsTraceUser_) {
    // for (var point in gpsTraceUser_.reversed) {
      timestampsUser.add(point.item1);
      model.add(timestampIndex.toDouble(), point.item1);
      timestampIndex++;
    }
    double mae = 0;
    List<double> modeledTimestamps = [];
    for (int index = 0; index < timestampIndex; index++) {
      modeledTimestamps.add(model.read(index.toDouble()));
      mae = mae + (modeledTimestamps[index] - timestampsUser[index]).abs();
    }
    mae = mae / timestampIndex;
    // debugPrint(timestampsUser.sublist(0,10).toString());
    // debugPrint(modeledTimestamps.sublist(0,10).toString());
//  debugPrint(model.getTimestamps().toString());
//  debugPrint(model.getModels().toString());

    // List<double> timestampsStep = [];
    // for(int i=1; i<timestampsUser.length; i++) {
    //   timestampsStep.add(timestampsUser[i]-timestampsUser[i-1]);
    // }
    // Stats stats = Stats(timestampsStep);

    double rawSize = timestampsUser.length.toDouble();
    double modelSize = model.getSize().toDouble();

    debugPrint('Number of points: $rawSize');
    debugPrint('Number of models: ${model
        .getModels()
        .length}');
    debugPrint('Size of model: $modelSize');
    debugPrint('Gain: ${(100*(rawSize-modelSize))/rawSize}%');
    // debugPrint('Distribution step values:');
    // debugPrint('min: ${stats.min} max: ${stats.max}');
    // debugPrint('average: ${stats.getMean()}');
    // // debugPrint('Frequencies:');
    // // debugPrint(stats.getFrecuencies()); // [[1, 3], [2, 2], [3, 3], [4, 3], [5, 3], [6, 4], [7, 2]]
    // debugPrint('median: ${stats.getMedian()}');
    // // debugPrint('variance: ${stats.getVariance()}');
    // debugPrint('standard deviation: ${stats.getStandardDeviation()}');
    // debugPrint('range: ${stats.getRange()}');
    debugPrint('mae: $mae');
    bool b = mae < 60;
    expect(b, true);
  });
}

/// Testing the POI attack with getTopDownPOI on a single user
void testGetTopDownPOI() {
  test('testing the POI attack with getTopDownPOI on a single user', () {
    double minTime = 60*5;
    double minDistance = 500;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    // List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'cabspotting',0);
    // List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'privamov-gps-user1-sampling10',1);
    List<Tuple3<double,double,double>> gpsTraceUser = readCSVUser(dir+'privamov-gps-user1',1);

    /// Results for privamov raw user 1
    /// Number of POI:
    /// Regular attack: 36
    /// getTopDownPOI: 26
    /// Duration time:
    /// Regular attack: 3413818ms -> 56min 53sec
    /// getTopDownPOI: 32214ms -> 32s -> x106

    final stopwatch = Stopwatch();
    stopwatch.start();
    var poiRegular = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
    stopwatch.stop();
    var t1 = stopwatch.elapsedMilliseconds;
    stopwatch.reset();
    stopwatch.start();
    var poiTopDown = POI.getTopDownPOI(gpsTraceUser, minTime, minDistance, minPts);
    stopwatch.stop();
    var t2 = stopwatch.elapsedMilliseconds;

    writeResult(Directory.current.path+'/results/privamovPOIRegularPrivamov1Raw.txt', poiRegular.toString());
    writeResult(Directory.current.path+'/results/privamovPOITopDownPrivamov1Raw.txt', poiTopDown.toString());

    debugPrint('Number of POI:');
    debugPrint('Regular attack: ${poiRegular.length}');
    debugPrint('getTopDownPOI: ${poiTopDown.length}');
    debugPrint('Duration time:');
    debugPrint('Regular attack: $t1');
    debugPrint('getTopDownPOI: $t2');
    expect(poiRegular.length == poiTopDown.length, true);
  });
}

/// Testing the POI attack with getTopDownPOI on a single user
void testGetTopDownPOICabspotting() {
  test('testing the POI attack with getTopDownPOI on a single user', () {
    double minTime = 60*5;
    double minDistance = 500;
    int minPts=2;
    var dir = Directory.current.path+'/datasets/';
    Map<int,List<Tuple3<double,double,double>>> gpsDataset = readCSV(dir+'cabspotting');
    bool b = true;
    int counterGood=0;
    for(var entry in gpsDataset.entries) {
      List<Tuple3<double, double, double>> gpsTraceUser = entry.value;

      final stopwatch = Stopwatch();
      stopwatch.start();
      var poiRegular = POI.getPOI(gpsTraceUser, minTime, minDistance, minPts);
      stopwatch.stop();
      var t1 = stopwatch.elapsedMilliseconds;
      stopwatch.reset();
      stopwatch.start();
      var poiTopDown = POI.getTopDownPOI(
          gpsTraceUser, minTime, minDistance, minPts);
      stopwatch.stop();
      var t2 = stopwatch.elapsedMilliseconds;

/*
      debugPrint('Number of POI:');
      debugPrint('Regular attack: ${poiRegular.length}');
      debugPrint('getTopDownPOI: ${poiTopDown.length}');
      debugPrint('Duration time:');
      debugPrint('Regular attack: $t1');
      debugPrint('getTopDownPOI: $t2');*/
      // if ((poiRegular.isEmpty || poiTopDown.isNotEmpty) && (t1 >= t2)) {
      if (t1 >= t2) {
        counterGood++;
      }
      b = b && (poiRegular.isEmpty || poiTopDown.isNotEmpty) && (t1 >= t2);
    }
    debugPrint('$counterGood results out of ${gpsDataset.length}');
    expect(counterGood == gpsDataset.length,true);
  });
}


void main() {
  // testStoreCSVArtificialDataset();
  // testPromesse();
  // testPromesseCabspotting();
  testTimestampModeling();
  // testREAD();
  // testGetTopDownPOI();
  // testGetTopDownPOICabspotting();
}